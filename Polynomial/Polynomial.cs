using System;
using System.Collections.Generic;
using System.Linq;
using PolynomialObject.Exceptions;

namespace PolynomialObject
{
    public sealed class Polynomial
    {
        public List<PolynomialMember> PolynomialMembersList { get; set; } = new List<PolynomialMember>();
        public double PolynomialVariable { get; set; }

        public Polynomial()
        {

        }

        public Polynomial(PolynomialMember member)
        {
            PolynomialMembersList.Add(member);
        }

        public Polynomial(IEnumerable<PolynomialMember> members)
        {
            foreach (var member in members)
            {
                PolynomialMembersList.Add(member);
            }
        }

        public Polynomial((double degree, double coefficient) member)
        {
            PolynomialMembersList.Add(new PolynomialMember(member.degree, member.coefficient));
        }

        public Polynomial(IEnumerable<(double degree, double coefficient)> members)
        {
            foreach ((double degree, double coefficient) in members)
            {
                PolynomialMembersList.Add(new PolynomialMember(degree, coefficient));
            }
        }

        /// <summary>
        /// The amount of not null polynomial members in polynomial 
        /// </summary>
        public int Count
        {
            get
            {
                int counter = 0;
                foreach (var polynomialMember in PolynomialMembersList)
                {
                    if (!(polynomialMember is null))
                    {
                        counter++;
                    }
                }
                return counter;
            }
        }

        /// <summary>
        /// The biggest degree of polynomial member in polynomial
        /// </summary>
        public double Degree
        {
            get
            {
                double maxDegree = 0;
                foreach (var polynomialMember in PolynomialMembersList)
                {
                    if (polynomialMember.Degree > maxDegree)
                    {
                        maxDegree = polynomialMember.Degree;
                    }
                }
                return maxDegree;
            }
        }

        /// <summary>
        /// Adds new unique member to polynomial 
        /// </summary>
        /// <param name="member">The member to be added</param>
        /// <exception cref="PolynomialArgumentException">Throws when member to add with such degree already exist in polynomial</exception>
        /// <exception cref="PolynomialArgumentNullException">Throws when trying to member to add is null</exception>
        public void AddMember(PolynomialMember member)
        {
            if (member is null)
            {
                throw new PolynomialArgumentNullException();
            }
            if (PolynomialMembersList.FirstOrDefault(x => x.Degree == member.Degree) == null && member.Coefficient != 0)
            {
                PolynomialMembersList.Add(member);
            }
            else
            {
                throw new PolynomialArgumentException();
            }
        }
        /// <summary>
        /// Adds new unique member to polynomial from tuple
        /// </summary>
        /// <param name="member">The member to be added</param>
        /// <exception cref="PolynomialArgumentException">Throws when member to add with such degree already exist in polynomial</exception>
        public void AddMember((double degree, double coefficient) member)
        {
            if (PolynomialMembersList.FirstOrDefault(x => x.Degree == member.degree) == null && member.coefficient != 0)
            {
                PolynomialMembersList.Add(new PolynomialMember(member.degree, member.coefficient));
            }
            else
            {
                throw new PolynomialArgumentException();
            }

        }



        /// <summary>
        /// Removes member of specified degree
        /// </summary>
        /// <param name="degree">The degree of member to be deleted</param>
        /// <returns>True if member has been deleted</returns>
        public bool RemoveMember(double degree)
        {
            try
            {
                this.PolynomialMembersList.Remove(PolynomialMembersList.First(x => x.Degree == degree));
                return true;
            }
            catch (InvalidOperationException)
            {
                return false;
            }
        }


        /// <summary>
        /// Removes member with 0 coeficient
        /// </summary>
        /// <returns>True if any member has been deleted</returns>
        public bool RemoveEmptyMember()
        {
            try
            {
                PolynomialMembersList.RemoveAll(x => x.Coefficient == 0);
                return true;
            }
            catch (InvalidOperationException)
            {
                return false;
            }
        }


        /// <summary>
        /// Searches the polynomial for a method of specified degree
        /// </summary>
        /// <param name="degree">Degree of member</param>
        /// <returns>True if polynomial contains member</returns>
        public bool ContainsMember(double degree)
        {
            return PolynomialMembersList.Contains(PolynomialMembersList.FirstOrDefault(x => x.Degree == degree));
        }

        /// <summary>
        /// Finds member of specified degree
        /// </summary>
        /// <param name="degree">Degree of member</param>
        /// <returns>Returns the found member or null</returns>
        public PolynomialMember Find(double degree)
        {
            return PolynomialMembersList.FirstOrDefault(x => x.Degree == degree);
        }

        /// <summary>
        /// Gets and sets the coefficient of member with provided degree
        /// If there is no null member for searched degree - return 0 for get and add new member for set
        /// </summary>
        /// <param name="degree">The degree of searched member</param>
        /// <returns>Coefficient of found member</returns>
        public double this[double degree]
        {
            get
            {
                return PolynomialMembersList.FirstOrDefault(x => x.Degree == degree).Coefficient;
            }
            set
            {
                bool flag = false;
                for (int i = 0; i < PolynomialMembersList.Count; i++)
                {
                    if (PolynomialMembersList[i].Degree == degree)
                    {
                        if (value != 0)
                        {
                            PolynomialMembersList[i].Coefficient = value;
                        }
                        else
                        {
                            PolynomialMembersList.Remove(PolynomialMembersList.First(x => x.Degree == degree));
                        }
                        flag = true;
                    }
                }
                if (!flag && value != 0)
                {
                    PolynomialMembersList.Add(new PolynomialMember(degree, value));
                }
            }
        }

        /// <summary>
        /// Convert polynomial to array of included polynomial members 
        /// </summary>
        /// <returns>Array with not null polynomial members</returns>
        public PolynomialMember[] ToArray()
        {
            return PolynomialMembersList.ToArray();
        }

        /// <summary>
        /// Adds two polynomials
        /// </summary>
        /// <param name="a">The first polynomial</param>
        /// <param name="b">The second polynomial</param>
        /// <returns>New polynomial after adding</returns>
        /// <exception cref="PolynomialArgumentNullException">Throws if either of provided polynomials is null</exception>
        public static Polynomial operator +(Polynomial a, Polynomial b)
        {

            if (a is null || b is null)
            {
                throw new PolynomialArgumentNullException();
            }
            Polynomial result = a;
            for (int i = 0; i < b.PolynomialMembersList.Count; i++)
            {
                if (result.PolynomialMembersList.FirstOrDefault(x => x.Degree == b.PolynomialMembersList[i].Degree) == null)
                {
                    result.PolynomialMembersList.Add(new PolynomialMember(b.PolynomialMembersList[i].Degree, b.PolynomialMembersList[i].Coefficient));
                }
                else
                {
                    result[b.PolynomialMembersList[i].Degree] = result[b.PolynomialMembersList[i].Degree] + b[b.PolynomialMembersList[i].Degree];
                }
            }
            result.RemoveEmptyMember();
            return result;
        }

        /// <summary>
        /// Adds polynomial and tuple
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The tuple</param>
        /// <returns>Returns new polynomial after adding</returns>
        public static Polynomial operator +(Polynomial a, (double degree, double coefficient) b)
        {
            Polynomial bPoli = new Polynomial(b);

            Polynomial result = a;
            for (int i = 0; i < bPoli.PolynomialMembersList.Count; i++)
            {
                if (result.PolynomialMembersList.FirstOrDefault(x => x.Degree == bPoli.PolynomialMembersList[i].Degree) == null)
                {
                    result.PolynomialMembersList.Add(new PolynomialMember(bPoli.PolynomialMembersList[i].Degree, bPoli.PolynomialMembersList[i].Coefficient));
                }
                else
                {
                    result[bPoli.PolynomialMembersList[i].Degree] = result[bPoli.PolynomialMembersList[i].Degree] + bPoli[bPoli.PolynomialMembersList[i].Degree];
                }
            }
            result.RemoveEmptyMember();
            return result;
        }

        /// <summary>
        /// Subtracts two polynomials
        /// </summary>
        /// <param name="a">The first polynomial</param>
        /// <param name="b">The second polynomial</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        /// <exception cref="PolynomialArgumentNullException">Throws if either of provided polynomials is null</exception>
        public static Polynomial operator -(Polynomial a, Polynomial b)
        {
            if (a is null || b is null)
            {
                throw new PolynomialArgumentNullException();
            }
            Polynomial result = new Polynomial(a.PolynomialMembersList);
            for (int i = 0; i < b.PolynomialMembersList.Count; i++)
            {
                if (result.PolynomialMembersList.FirstOrDefault(x => x.Degree == b.PolynomialMembersList[i].Degree) == null)
                {
                    result.PolynomialMembersList.Add(new PolynomialMember(b.PolynomialMembersList[i].Degree, -b.PolynomialMembersList[i].Coefficient));
                }
                else
                {
                    result[b.PolynomialMembersList[i].Degree] = result[b.PolynomialMembersList[i].Degree] - b[b.PolynomialMembersList[i].Degree];
                }
            }
            result.RemoveEmptyMember();
            return result;
        }

        /// <summary>
        /// Subtract polynomial and tuple
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The tuple</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        public static Polynomial operator -(Polynomial a, (double degree, double coefficient) b)
        {
            Polynomial bPoli = new Polynomial(b);

            Polynomial result = new Polynomial(a.PolynomialMembersList);
            for (int i = 0; i < bPoli.PolynomialMembersList.Count; i++)
            {
                if (result.PolynomialMembersList.FirstOrDefault(x => x.Degree == bPoli.PolynomialMembersList[i].Degree) == null)
                {
                    result.PolynomialMembersList.Add(new PolynomialMember(bPoli.PolynomialMembersList[i].Degree, -bPoli.PolynomialMembersList[i].Coefficient));
                }
                else
                {
                    result[bPoli.PolynomialMembersList[i].Degree] = result[bPoli.PolynomialMembersList[i].Degree] - bPoli[bPoli.PolynomialMembersList[i].Degree];
                }
            }
            result.RemoveEmptyMember();
            return result;
        }


        /// <summary>
        /// Multiplies two polynomials
        /// </summary>
        /// <param name="a">The first polynomial</param>
        /// <param name="b">The second polynomial</param>
        /// <returns>Returns new polynomial after multiplication</returns>
        /// <exception cref="PolynomialArgumentNullException">Throws if either of provided polynomials is null</exception>
        public static Polynomial operator *(Polynomial a, Polynomial b)
        {
            if (a is null || b is null)
            {
                throw new PolynomialArgumentNullException();
            }

            Polynomial result = new Polynomial();
            for (int i = 0; i < a.PolynomialMembersList.Count; i++)
            {
                for (int j = 0; j < b.PolynomialMembersList.Count; j++)
                {
                    PolynomialMember newMember = new PolynomialMember(a.PolynomialMembersList[i].Degree + b.PolynomialMembersList[j].Degree, a.PolynomialMembersList[i].Coefficient * b.PolynomialMembersList[j].Coefficient);
                    try
                    {              
                        result.AddMember(newMember);
                    }  
                    catch (PolynomialArgumentException) when (newMember.Coefficient == 0)
                    {
                        result.PolynomialMembersList.Add(newMember);
                    }
                    catch (PolynomialArgumentException)     
                    {
                        result[a.PolynomialMembersList[i].Degree + b.PolynomialMembersList[j].Degree] += a.PolynomialMembersList[i].Coefficient * b.PolynomialMembersList[j].Coefficient;
                    }
                }
            }

            result.RemoveEmptyMember();
            return result;
        }

        /// <summary>
        /// Multiplies polynomial and tuple
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The tuple</param>
        /// <returns>Returns new polynomial after multiplication</returns>
        public static Polynomial operator *(Polynomial a, (double degree, double coefficient) b)
        {
            Polynomial bPoli = new Polynomial(b);

            Polynomial result = new Polynomial();
            for (int i = 0; i < a.PolynomialMembersList.Count; i++)
            {
                for (int j = 0; j < bPoli.PolynomialMembersList.Count; j++)
                {
                    PolynomialMember newMember = new PolynomialMember(a.PolynomialMembersList[i].Degree + bPoli.PolynomialMembersList[j].Degree, a.PolynomialMembersList[i].Coefficient * bPoli.PolynomialMembersList[j].Coefficient);
                    try
                    {
                        result.AddMember(newMember);
                    }
                    catch (PolynomialArgumentException) when (newMember.Coefficient == 0)
                    {
                        result.PolynomialMembersList.Add(newMember);
                    }
                    catch (PolynomialArgumentException)
                    {
                        result[a.PolynomialMembersList[i].Degree + bPoli.PolynomialMembersList[j].Degree] += a.PolynomialMembersList[i].Coefficient * bPoli.PolynomialMembersList[j].Coefficient;
                    }
                }
            }
            result.RemoveEmptyMember();
            return result;
        }

        /// <summary>
        /// Adds polynomial to polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial to add</param>
        /// <returns>Returns new polynomial after adding</returns>
        /// <exception cref="PolynomialArgumentNullException">Throws if provided polynomial is null</exception>
        public Polynomial Add(Polynomial polynomial)
        {
            return this + polynomial;
        }

        /// <summary>
        /// Adds tuple to polynomial
        /// </summary>
        /// <param name="member">The tuple to add</param>
        /// <returns>Returns new polynomial after adding</returns>
        public Polynomial Add((double degree, double coefficient) member)
        {
            return this + member;
        }

        /// <summary>
        /// Subtracts polynomial from polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial to subtract</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        /// <exception cref="PolynomialArgumentNullException">Throws if provided polynomial is null</exception>
        public Polynomial Subtraction(Polynomial polynomial)
        {
            return this - polynomial;
        }

        /// <summary>
        /// Subtracts tuple from polynomial
        /// </summary>
        /// <param name="member">The tuple to subtract</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        public Polynomial Subtraction((double degree, double coefficient) member)
        {
            return this - member;
        }

        /// <summary>
        /// Multiplies polynomial with polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial for multiplication </param>
        /// <returns>Returns new polynomial after multiplication</returns>
        /// <exception cref="PolynomialArgumentNullException">Throws if provided polynomial is null</exception>
        public Polynomial Multiply(Polynomial polynomial)
        {
            return this * polynomial;
        }

        /// <summary>
        /// Multiplies tuple with polynomial
        /// </summary>
        /// <param name="member">The tuple for multiplication </param>
        /// <returns>Returns new polynomial after multiplication</returns>
        public Polynomial Multiply((double degree, double coefficient) member)
        {
            return this * member;
        }
    }
}
